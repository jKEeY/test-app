﻿// MainWindow.xaml.cs
using System.Windows;
using System.Diagnostics;
using Dota2AHKLauncher.ViewModels;

namespace Dota2AHKLauncher
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
            Debug.WriteLine("MainWindow DataContext set to MainViewModel");
        }
    }
}
