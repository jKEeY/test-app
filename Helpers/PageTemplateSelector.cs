﻿using System.Windows;
using System.Windows.Controls;

namespace Dota2AHKLauncher.Helpers
{
    public class PageTemplateSelector : DataTemplateSelector
    {
        public DataTemplate MainPageTemplate { get; set; }
        public DataTemplate HeroesPageTemplate { get; set; }
        public DataTemplate SettingsPageTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is string pageKey)
            {
                switch (pageKey)
                {
                    case "MainPage":
                        return MainPageTemplate;
                    case "HeroesPage":
                        return HeroesPageTemplate;
                    case "SettingsPage":
                        return SettingsPageTemplate;
                }
            }
            return base.SelectTemplate(item, container);
        }
    }
}
