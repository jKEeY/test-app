﻿using System.Windows;
using Dota2AHKLauncher.ViewModels;

namespace Dota2AHKLauncher
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindow window = new MainWindow();
            MainViewModel viewModel = new MainViewModel();
            window.DataContext = viewModel;
            window.Show();
        }
    }
}
