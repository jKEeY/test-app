﻿// HeroesPage.xaml.cs
using System.Windows.Controls;
using Dota2AHKLauncher.ViewModels;
using System.Diagnostics;

namespace Dota2AHKLauncher.Views
{
    public partial class HeroesPage : UserControl
    {
        public HeroesPage()
        {
            InitializeComponent();
            Debug.WriteLine("HeroesPage constructor called"); // Лог для проверки вызова конструктора
            DataContext = ((MainViewModel)((MainWindow)System.Windows.Application.Current.MainWindow).DataContext);
            Debug.WriteLine("HeroesPage DataContext set"); // Лог для проверки установки контекста данных
        }
    }
}
