﻿// MainViewModel.cs
using System.Collections.ObjectModel;
using System.Windows.Input;
using Dota2AHKLauncher.Helpers;
using Dota2AHKLauncher.Models;

namespace Dota2AHKLauncher.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private string _currentPage;
        private ObservableCollection<Hero> _heroes;
        private Hero _selectedHero;

        public MainViewModel()
        {
            NavigateCommand = new RelayCommand(Navigate);
            CurrentPage = "MainPage";
            LoadHeroes();
        }

        public string CurrentPage
        {
            get => _currentPage;
            set
            {
                _currentPage = value;
                OnPropertyChanged(nameof(CurrentPage));
            }
        }

        public ObservableCollection<Hero> Heroes
        {
            get => _heroes;
            set
            {
                _heroes = value;
                OnPropertyChanged(nameof(Heroes));
            }
        }

        public Hero SelectedHero
        {
            get => _selectedHero;
            set
            {
                _selectedHero = value;
                OnPropertyChanged(nameof(SelectedHero));
            }
        }

        public ICommand NavigateCommand { get; }

        private void Navigate(object parameter)
        {
            if (parameter is string page)
            {
                CurrentPage = page;
            }
        }

        private void LoadHeroes()
        {
            Heroes = new ObservableCollection<Hero>
            {
                new Hero { Name = "ABADDON", ImagePath = "/Images/Heroes/ABADDON.png" },
                new Hero { Name = "AXE", ImagePath = "/Images/Heroes/AXE.png" }
            };
        }

        public ICommand SelectHeroCommand => new RelayCommand<Hero>(hero =>
        {
            SelectedHero = hero;
            CurrentPage = "HeroSettingsPage";
        });
    }
}
