﻿namespace Dota2AHKLauncher.Models
{
    public class Hero
    {
        public string Name { get; set; }
        public string ImagePath { get; set; }
    }
}
