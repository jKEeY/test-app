﻿namespace Dota2AHKLauncher.Models
{
    public class Setting
    {
        public string Name { get; set; }
        public string KeyBind { get; set; }
        public string Description { get; set; }
    }
}
