﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Dota2AHKLauncher.Converters
{
    public class PageToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return Brushes.Transparent;

            return value.ToString() == parameter.ToString() ? Brushes.LightGray : Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
